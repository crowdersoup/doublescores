<?php
/**
 * doublescores functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package doublescores
 */

if ( ! function_exists( 'doublescores_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function doublescores_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on doublescores, use a find and replace
		 * to change 'doublescores' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'doublescores', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'doublescores' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'doublescores_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'doublescores_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function doublescores_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'doublescores_content_width', 640 );
}
add_action( 'after_setup_theme', 'doublescores_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function doublescores_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'doublescores' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'doublescores' ),
		'before_widget' => '<section id="%1$s" class="mb-2 border border-gray-400 text-sm p-4 widget text-gray-700 bg-blue-100 %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="text-lg">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'doublescores_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function doublescores_scripts() {
	wp_enqueue_style( 'doublescores-style', get_stylesheet_uri() );

	wp_enqueue_script( 'doublescores-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'doublescores-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'doublescores_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Embed handler for video links in posts.
 *
 * When a youtube or invidio.us link is included in the post content, this will embed the corresponding
 * video in an invidio.us iframe.
 *
 * TODO: could perhaps be moved in to a plugin, so that it doesn't need to be copied between themes.
 */
wp_embed_register_handler( 'invidious_watch', '#https?://(www.)?invidio\.us/watch\?v=([A-Za-z0-9\-_]+)#i', 'doublescores_embed_handler_video' );
wp_embed_register_handler( 'invidious_embed', '#https?://(www.)?invidio\.us/embed/([A-Za-z0-9\-_]+)#i', 'doublescores_embed_handler_video' );
wp_embed_register_handler( 'youtube_watch', '#https?://(www.)?youtube\.com/watch\?v=([A-Za-z0-9\-_]+)#i', 'doublescores_embed_handler_video' );
wp_embed_register_handler( 'youtube_embed', '#https?://(www.)?youtube\.com/embed/([A-Za-z0-9\-_]+)#i', 'doublescores_embed_handler_video' );
function doublescores_embed_handler_video( $matches, $attr, $url, $rawattr ) {
    $video_id = $matches[2];

    $img_url = 'https://img.youtube.com/vi/'.$video_id.'/maxresdefault.jpg';
    $invidious_url = 'https://invidious.snopyta.org/watch?v='.$video_id;
    $invidious_embed = 'https://invidious.snopyta.org/embed/'.$video_id;
    $youtube_url = 'https://youtube.com/watch?v='.$video_id;
    $info_url = 'https://commonplace.doubleloop.net/20200508100100-video_embeds';

    $embed = sprintf(
        '<div class="w-full embed-responsive aspect-ratio-4/3"><iframe src="%1$s" frameborder="0" scrolling="no" marginwidth="0" marginheight="0"></iframe></div><div>watch on: <a href="%2$s" target="_blank">invidious</a> | <a href="%3$s" target="_blank">youtube</a>.  (<a href="%4$s">?</a>)</div>',
        $invidious_embed, $invidious_url, $youtube_url, $info_url);

    return $embed;
}

//<img src="%1$s">

function doublescores_embed_defaults($embed_size){
    $embed_size['width'] = 460;
    return $embed_size;
}
add_filter('embed_defaults', 'doublescores_embed_defaults');

// To try and fix microformats of reply posts.
// see: https://github.com/ngm/doubleloop.net/issues/4
add_filter('the_content', 'wrapPostContentWithContentMicroformats', 5, 2);
function wrapPostContentWithContentMicroformats($content) {
    return '<div class="e-content">' . $content . '</div>';
}

/**
 * Fixes issues with fixed width image captions breaking mobile responsiveness.
 *
 * See: https://wordpress.stackexchange.com/a/310339
 */
function remove_img_caption_width($width, $atts, $content) {
    return 0;
}

add_filter('img_caption_shortcode_width', 'remove_img_caption_width', 10, 3);
