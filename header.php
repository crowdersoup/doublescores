<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package doublescores
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>

  <link href="https://fonts.googleapis.com/css?family=Nunito:400,700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
  <style>
   body {
       font-family: 'Nunito', sans-serif;
   }
  </style>
</head>

<body <?php body_class('bg-gray-100'); ?>>
<div class="bg-black text-center py-2">
    <a target="_blank" class="text-white underline text-xl hover:no-underline" href="https://blacklivesmatter.com/">#BlackLivesMatter</a>
</div>
<div id="page" class="container mx-auto">
	  <!-- <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'doublescores' ); ?></a> -->

	<header id="masthead" class="site-header py-4">
		<div class="flex flex-col sm:flex-row sm:items-baseline sm:justify-between">
			<?php the_custom_logo(); ?>
            <h1 class="text-lg sm:text-2xl w-full sm:w-1/2 sm:pl-2 ml-5"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
            <?php
			$doublescores_description = get_bloginfo( 'description', 'display' );
			if ( $doublescores_description || is_customize_preview() ) :
				?>
				    <p class="text-lg w-full sm:w-1/2 sm:text-right ml-5 sm:ml-0 mr-1"><?php echo $doublescores_description; /* WPCS: xss ok. */ ?></p>
			<?php endif; ?>
		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'doublescores' ); ?></button>
			<?php
			wp_nav_menu( array(
				'theme_location' => 'menu-1',
				'menu_id'        => 'primary-menu',
			) );
			?>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<div id="content" class="flex flex-col sm:flex-row">
