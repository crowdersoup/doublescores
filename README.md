# doublescores

This is a WordPress theme that (tries to) work with the [IndieWeb plugins](https://indieweb.org/WordPress/Plugins).

It's built with the [_s](https://underscores.me/) WordPress starter theme and [tailwindcss](https://tailwindcss.com/).

It's unfinished and a little hacky, so might not be the most robust or stable theme to use, but feel free to give it a try!  

You can see it in action at [doubleloop.net](https://doubleloop.net).
