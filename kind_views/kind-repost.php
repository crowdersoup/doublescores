<?php
/*
  Default Template
 *	The Goal of this Template is to be a general all-purpose model that will be replaced by customization in other templates
 */

$author = array();
if ( isset( $cite['author'] ) ) {
	$author = Kind_View::get_hcard( $cite['author'] );
}
$url = '';
if ( isset( $cite['url'] ) ) {
	$url = $cite['url'];
}
$site_name = Kind_View::get_site_name( $cite );
$title     = Kind_View::get_cite_title( $cite );
$embed     = self::get_embed( $url );
$duration  = $mf2_post->get( 'duration', true );
if ( ! $duration ) {
		$duration = calculate_duration( $mf2_post->get( 'dt-start' ), $mf2_post->get( 'dt-end' ) );
}
$rsvp = $mf2_post->get( 'rsvp', true );

if ( ! $kind ) {
	return;
}

// Add in the appropriate type
if ( ! empty( $type ) ) {
	$type = ( empty( $url ) ? 'p-' : 'u-' ) . $type;
}
?>

<section class="h-cite <?php echo $type; ?> ">
<section class="kind-meta-section">
<header class="kind-meta-section__header">
<i class="fas fa-sync px-1"></i><?php
echo 'Reposted: ';
if ( $title ) {
    echo $title;
}
if ( ! empty( $author ) ) {
    echo ' ' . __( 'by', 'indieweb-post-kinds' ) . ' ' . $author;
}
if ( $site_name ) {
    echo '<em> (' . $site_name . ')</em>';
}
?>
</header>
<?php
if ( $cite && is_array( $cite ) ) {
	if ( $embed ) {
		echo sprintf( '<blockquote class="e-summary">%1s</blockquote>', $embed );
	} elseif ( array_key_exists( 'summary', $cite ) ) {
		echo sprintf( '<blockquote class="e-summary">%1s</blockquote>', $cite['summary'] );
	}
}

// Close Response
?>
</section>

<?php
