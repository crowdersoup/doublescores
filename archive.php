<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package doublescores
 */

get_header();
?>

    <div id="primary" class="w-full sm:w-3/4">
        <main id="main" class="flex flex-wrap">

		<?php if ( have_posts() ) : ?>

            <header class="page-header bg-gray-300 text-gray-700 px-6 py-2 rounded border-solid border-0 border-t-2 border-b-2 sm:border-2 font-bold border-dashed border-gray-500 w-full mb-6 flex-col flex sm:flex-row leading-normal tracking-tight text-lg shadow-md">
				<?php
				the_archive_title( '<h1 class="page-title text-xl">', '</h1>' );
				?>
			</header><!-- .page-header -->

			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_type() );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
