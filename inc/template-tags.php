<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package doublescores
 */

if ( ! function_exists( 'doublescores_posted_on' ) ) :
	/**
	 * Prints HTML with meta information for the current post-date/time.
	 */
	function doublescores_posted_on() {
		$time_string = '<time class="text-base px-2 bg-gray-300 text-gray-700 tracking-wide entry-date published updated dt-published dt-updated" datetime="%1$s">%2$s</time>';

		/*if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="text-base bg-gray-300 text-gray-700 px-2 tracking-wide entry-date published dt-published" datetime="%1$s">%2$s</time>';
		}*/

		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( DATE_W3C ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( DATE_W3C ) ),
			esc_html( get_the_modified_date() )
		);

		$posted_on = sprintf(
			/* translators: %s: post date. */
			esc_html_x( '%s', 'post date', 'doublescores' ),
			'<a class="no-underline u-url" href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
		);

		echo '<div class="posted-on mb-1">' . $posted_on . '</div>'; // WPCS: XSS OK.

	}
endif;

if ( ! function_exists( 'doublescores_posted_by' ) ) :
	/**
	 * Prints HTML with meta information for the current author.
	 */
	function doublescores_posted_by() {
        $author_id = get_the_author_meta( 'ID' );
        $author_url = esc_url( get_author_posts_url( $author_id ) );
        $author_name = esc_html( get_the_author() );
        $author_avatar_url = esc_url( get_avatar_url( $author_id ) );

		$byline = sprintf(
			/* translators: %s: post author. */
            esc_html_x( '%s', 'post author', 'doublescores' ),
            '<a class="h-card u-author" href="' . $author_url . '"><span class="p-name">' . $author_name . '</span><img class="u-photo" src="' . $author_avatar_url . '"></a>'
        );

		echo '<div class="byline hidden"> ' . $byline . '</div>'; // WPCS: XSS OK.

	}
endif;

if ( ! function_exists( 'doublescores_entry_footer' ) ) :
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function doublescores_entry_footer() {
		// Hide category and tag text for pages.
		if ( 'post' === get_post_type() ) {
			/* translators: used between list items, there is a space after the comma */
			$categories_list = get_the_category_list( esc_html__( '', 'doublescores' ) );
			if ( $categories_list ) {
				/* translators: 1: list of categories. */
				printf( '<div class="cat-links mb-1">' . esc_html__( '%1$s', 'doublescores' ) . '</div>', $categories_list ); // WPCS: XSS OK.
			}

			/* translators: used between list items, there is a space after the comma */
			$tags_list = get_the_tag_list( '<ul><li>', '</li><li>', '</li></ul>' );
			if ( $tags_list ) {
				/* translators: 1: list of tags. */
				printf( '<div class="tags-links mb-1">' . esc_html__( '%1$s', 'doublescores' ) . '</div>', $tags_list ); // WPCS: XSS OK.
			}
		}

		if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
			echo '<div class="comments-link">';
			comments_popup_link(
				sprintf(
					wp_kses(
						/* translators: %s: post title */
						__( 'Leave a comment', 'doublescores' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				), '1 interaction', '% interactions'
			);
			echo '</div>';
		}

		edit_post_link(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Edit <span class="screen-reader-text">%s</span>', 'doublescores' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			),
			'<span class="edit-link">',
			'</span>'
		);
	}
endif;

if ( ! function_exists( 'doublescores_post_thumbnail' ) ) :
	/**
	 * Displays an optional post thumbnail.
	 *
	 * Wraps the post thumbnail in an anchor element on index views, or a div
	 * element when on single views.
	 */
	function doublescores_post_thumbnail() {
		if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
			return;
		}

		if ( is_singular() ) :
			?>

			<div class="post-thumbnail">
				<?php the_post_thumbnail(); ?>
			</div><!-- .post-thumbnail -->

		<?php else : ?>

		<a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true" tabindex="-1">
			<?php
			the_post_thumbnail( 'post-thumbnail', array(
				'alt' => the_title_attribute( array(
					'echo' => false,
				) ),
			) );
			?>
		</a>

		<?php
		endif; // End is_singular().
	}
endif;
